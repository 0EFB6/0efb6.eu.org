---
title: "Python"
date: "2022-11-06"
author: "Wilson Chang"
description: "Reference for Python."
---

Reference for Python.

## PyGame
- https://www.geeksforgeeks.org/python-moving-an-object-in-pygame/
- https://github.com/search?q=pygame.image.load&type=Code&l=Python
- https://www.pygame.org/docs/ref/event.html
- https://self-learning-java-tutorial.blogspot.com/2015/12/pygame-setting-background-image.html
## Python Chat Server/Email Server
- https://automatetheboringstuff.com/chapter16/
- https://www.geeksforgeeks.org/python-program-that-sends-and-recieves-message-from-client/
- https://towardsdatascience.com/build-a-simple-real-life-chat-app-with-python-a3ce8aebccb0

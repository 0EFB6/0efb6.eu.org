---
title: "Reference for OpenCV and Tensorflow"
date: "2022-11-06"
author: "Wilson Chang"
description: "Reference for OpenCV and Tensorflow"
---

Reference for OpenCV and Tensorflow

## Reference
- Car Plate Recognition [Youtube Link](https://www.youtube.com/channel/UCxAnMtjN08ryThpgYTBmILg)
- Train custom yolov4 weights [Youtube Link](https://www.youtube.com/watch?v=mmj3nxGT2YQ)
- OpeCV Amazing Threshold [Youtube Link](https://www.youtube.com/watch?v=jXzkxsT9gxM)

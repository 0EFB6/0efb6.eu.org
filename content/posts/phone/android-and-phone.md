---
title: "Android And Phone"
date: "2022-11-06"
author: "Wilson Chang"
description: "Article about Android phones and some tips and tricks."
---

Article about Android phones and some tips and tricks.

## Phone
### Samsung
Samsung Galazy Mini S5570 [Specification](https://www.gsmarena.com/samsung_galaxy_mini_s5570-3725.php)
## App Store
### Google Play Store
1) Naptime `Real Battery Saver`
2) Discord
### FDriod Apps
1) Last Launcher [Link](https://f-droid.org/en/packages/io.github.subhamtyagi.lastlauncher/)
2) Aegis `OTP App`
### Aurora Store
- Access to play store apps
- [Simple Mobile Tool Apps](https://simplemobiletools.com/)
## Operating System
1) Lineage [Official Website](https://lineageos.org/)
2) eOS [Official Website](https://e.foundation/)
3) CalyxOS [Official Website](https://calyxos.org/)
4) Graphene OS [Official Website](https://grapheneos.org/)
---
title: "Debloat Windows 10 or 11"
date: "2022-10-25"
author: "Wilson Chang"
description: "This guide shows you how to debloat windows on Windows 10 or 11."
---

This guide shows you how to debloat windows on Windows 10 or 11.

{{< image src="/image/debloat-windows-10or11.png" alt="Failed to load image." position="center" style="border-radius: 8px;" >}}

## Guide

1. Open `Windows Power Shell` as **admin**.
2. Paste the following command and run it.

### Code
```
iwr -useb https://christitus.com/win | iex
```

----
## Reference
This tool is created by Chris Titus Tech.
- https://youtu.be/tPRv-ATUBe4
- https://christitus.com/windows-tool/

---
title: "Crack Microsoft Office"
date: "2022-10-25"
author: "Wilson Chang"
description: "This guide shows you how to download and install Microsoft Office for free, and also activate it for free."
---

This guide shows you how to download and install Microsoft Office for free, and also activate it for free.

{{< image src="/image/crack-msoffice.jpg" alt="Failed to load image." position="center" style="border-radius: 8px;" >}}

## Guide

1. Download the files from `Reference`.
2. Extract the files.
3. Disable internet connection.
4. Run the executable file to install MS Office.
5. Enable internet connection.
6. Run the activator to activate MS Office.
----
## Reference
- https://mega.nz/folder/ZL5kgJyb#GLRhB8wclZhd1Ibfu7gzGw

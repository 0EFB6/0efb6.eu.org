---
title: "Windows Applications"
date: "2022-11-06"
author: "Wilson Chang"
description: "List of applications that are useful on Windows."
---

List of applications that are useful on Windows.

## Gaming
- Steam
- Bluestack
- Wang Zhe Rong Yao

## System Monitoring
- Hwinfo64
- QuickCPU
- CoreTemp
---
title: "Cybersecurity Notes"
date: "2023-07-24"
author: "Wilson Chang"
description: "Key notes and useful platform for learning cybersecurity"
---

10:57 | 2023-07-24 | Wilson Chang

Key notes and platforms for cybersecurity.

# Notes

### Cybersecurity Learning Platforms
- Pico CTF
- Port Swigger academy
- Hacker 101

### Bug Bounty
- Hacker One
- Bugcrowd
- Intigriti
- Synack

# Reference

N/A

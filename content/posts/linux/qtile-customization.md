---
title: "Qtile Customization"
date: "2022-11-06"
author: "Wilson Chang"
description: "This guide shows you how to customize Qtile."
---

This guide shows you how to customize Qtile.

## Reference
- https://youtu.be/7fKC7s-9f8E

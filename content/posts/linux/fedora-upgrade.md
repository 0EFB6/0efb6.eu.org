---
title: "Upgrade To Fedora 37 From Fedora 36"
date: "2022-10-27"
author: "Wilson Chang"
description: "This guide shows you how to upgrade to Fedora 37 from Fedora 36 as Fedora 37 is now in Beta."
---

This guide shows you how to upgrade to Fedora 37 from Fedora 36 as Fedora 37 is now in Beta.

{{< image src="/image/fedora-upgrade.png" alt="Failed to load image." position="center" style="border-radius: 8px;" >}}


# Guide
----

### Update Fedora 36 System
```
sudo dnf upgrade --refresh
```

### Remove Old Package
```
sudo dnf autoremove
```

### Upgrade To Fedora 37
```
sudo dnf install dnf-plugin-system-upgrade -y
sudo dnf system-upgrade download --releasever=37
```

>If you encounter any issues during this step, add `--alowerasing` to your command.
>
>```sudo dnf system-upgrade download --releasever=37 --allowerasing```
>
>**OR** 
>
>```sudo dnf distro-sync```

&nbsp;
### Reboot Your System To Complete Upgrade
```
sudo dnf system-upgrade reboot
```

### Post Upgrade Cleanup
```
sudo dnf system-upgrade clean
```

### Remove broken symlinks
```sudo symlinks -r -d /usr``` **OR** ```sudo symlinks -r /usr | grep dangling```

----

# Reference
- https://www.linuxcapable.com/how-to-upgrade-fedora-36-to-fedora-37-with-gnome-43/

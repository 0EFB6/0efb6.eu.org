---
title: "Disk Encryption"
date: "2023-08-22"
author: "Wilson Chang"
description: "Short notes / cheatsheet for encrypting disk using luks and auto mount them."
---

Short notes / cheatsheet for disk encryption in Linux.

## Configuration For Auto Mounting LUKS Partition in Crypttab
```
cryptdata UUID=<ENTER UUID HERE> none luks
secret	<PARTITION>	<PATH TO SECRET>
```

## Commands For Creating LUKS Encrypted Partition
```
cryptsetup -y -v luksFormat <PARTITION>
cryptsetup open <PARTITION> cryptroot (or any name you prefer)
mkfs.ext4 -L <LABEL> /dev/mapper/cryptroot (or any name you prefer)
mount -t ext4 /dev/mapper/cryptroot <MOUNTPOINT>
cryptsetup luksClose cryptroot
```

## Commands For Expanding LUKS Encrypted Partition
```
cryptsetup status cryptdata
lvextend -L +100G /dev/mapper/NAME
lvs
cryptsetup status cryptdata
resize2fs -p /dev/mapper/NAME
lvs
df -h /
```


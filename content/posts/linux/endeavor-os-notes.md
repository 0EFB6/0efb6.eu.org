---
title: "Endeavor OS Notes"
date: "2022-11-03"
author: "Wilson Chang"
description: "This guide shows you how to customize Endeavor OS. Furthermore, there is a list of recommended applications to install ion a freshly Endeavor OS system.This guide shows you how to customize Endeavor OS. Furthermore, there is a list of recommended applications to install ion a freshly Endeavor OS system."
---

This guide shows you how to customize Endeavor OS. Furthermore, there is a list of recommended applications to install ion a freshly Endeavor OS system.

## Ricing Theme
`oomox-Pandora-lighter`

## Icon Pack
`candy-icons`

## Applications 
- Keepassxc
- Steam
- Anydesk
- mpv
- VNC
- Spotify
- Virt Manager
- Libreoffice
- Discord
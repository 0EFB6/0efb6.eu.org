---
title: "Xorg on Debian"
date: "2022-10-25"
author: "Wilson Chang"
description: "Guidance on how to install xorg and xserver on Debian GNU/Linux."
---

Guidance on how to install xorg and xserver on Debian GNU/Linux.

{{< image src="/image/xorg-debian.jpg" alt="Failed to load image." position="center" style="border-radius: 8px;" >}}

## Code

```
sudo apt install xserver-xorg-video-intel xserver-xorg-core xserver-xorg xorg
```


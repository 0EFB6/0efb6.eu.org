---
title: "DWM on Arch Linux"
date: "2022-11-26"
author: "Wilson Chang"
description: "Guidance on how to install DWM Window Manager on Arch Linux."
---

Guidance on how to install DWM Window Manager on Arch Linux.

{{< image src="/image/dwm-debian.png" alt="Failed to load image." position="center" style="border-radius: 8px;" >}}

## Code

```
# Prerequisite
sudo pacman -Sy ttf-hack ttf-joypixels libx11 libxinerama libxft gst-libav gst-plugins-good xorg-server xorg-xinit

# Recommended Programs To Be Installed
sudo apt install lxappearance gpm papirus-icon-theme ranger feh p7zip unrar ffmpeg obs-studio pcmanfm adb fastboot
```

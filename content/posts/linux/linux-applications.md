---
title: "Linux Applications"
date: "2022-11-06"
author: "Wilson Chang"
description: "List of applications that are useful on Linux."
---

List of applications that are useful on Linux.

## Package Manager / Font Manager
- Pacfinder
- Font Downloader
## Browser
- Brave
- Firefox
- Chromium
## Password Manager
- Keepassxc
## Messaging / Chat / Privacy Oriented App
- Discord
- IRC Chat Client
- Qtox / Toxic
- Session
- Briar
- Matrix / Element
- Mastodon `Decentralized Social Platform`
- ModernDeck (Twitter Client) [Website](https://moderndeck.org/)
## Video Conferencing App
- Jitsi
- Calyx Meet
- Bigbluebutton
## Remote Connection
- Realvnc
- Nomachine
- Anydesk
## Music & Video Player
- MPV
- VLC
- Spotify
- Audacious
- Clementine
- Lollypop
- Nuclear Music Player
- Mousai `Shazam for Linux`
- Poddr `Podcasts`
- Rofi Beats #git
- Musikcube #cli
- ncmpcpp #cli
- Navidrome #selfhost
- Jellyfin #selfhost
## Multimedia (Photo/Audio/Video/Recording/Encoder)
- OBS
- GIMP
- Ardour
- Audacium
- Handbrake
- Kdenlive
- Simplescreenrecorder
- Rapid Photo Downloader
- Ristretto Image Viewer
- Gthumb Image Viewer
- Stremio `Streaming Video/Movie`
- Gallery-dl #cli 
- Photoprism #selfhost 
- Piwigo #selfhost 
## File Sharing/Manager/Cleaner/Archive Manager
- Fsearch
- Syncthing `File Synchronization`
- Motrix `Download Manager`
- Peazip `Archive Manager`
- Freefilesync `File Synchronization`
- Metadata Cleaner `Clean Metadata in Files`
- Czkawka `Clean Duplicate Files`
- Dupeguru `Duplicate Files Cleaner`
- rmlint `Duplicate Files Cleaner`
- Zoxide `Alternative For cd`
## Mail
- Neomutt
- StartMail
- Tutanota
- ProtonMail
- FastMail
- CTemplar
## VPN
- Proton VPN
- Surfshark VPN
- Tunnel Bear VPN
- Private Internet Access
## Calculator
- Qalculate
## Note Taking/Office Suite
- Obsedian
- Zotero
- When `CLI Calendar` #cli 
- Sleek `Todo List`
- CryptPad `Online Office Suite`
- Mark Text [Markdown Editor](https://github.com/marktext/marktext)
- Manuskript  [Website](http://www.theologeek.ch/manuskript/)
- Logseq [Knowledge Management](https://logseq.com/)
- Did You Mean [Spelling checker](https://github.com/hisbaan/didyoumean)
- Boost Note [For Developer](https://boostnote.io/)
- Supernotes [Proprietary](https://supernotes.app/)
- Turtl [Open Source Note Taking App](https://turtlapp.com/features/)
## USB Writer
- Ventoy
- Rufus
- Popsicle
- BalenaEtcher
- DD Command  `dd if=XXX.iso of=/dev/sdX bs=4M status="progress" && sync`
## Disk Usage Analyzer / File Rescue
- Baobab
- Dutree
- Testdisk
- f3 - Fight Flash Fraud
## Key Binding
- SXHKD
## Window Manager
- DWM
- TWM
- JWM
- i3 WM
- qtile
- Xmonad
- Awesome WM
- Bspwm
- Sway
- Left WM
## Browser Extension
- UBlockorigin
- Team Tube Buddy
- Dark Reader
- No awards for Reddit
- Reddit Enhancement Suite
- Stack Overflow Prettifier
- DF YouTube (Distraction Free)
- GitHub Defreshed
- Disable Polymer on YouTube
- Fast YouTube Load
- Chameleon
- Click & Clean
- Get RSS Feed URL
- Authenticator
- Momentum
- GoFullPage
- Midnight Lizard
- Notifier for GitHub
- Extensity
### CRX Downloader
Extension to download CRX extension files: [Extension Website](https://chrome.google.com/webstore/detail/crx-extractordownloader/ajkhmmldknmfjnmeedkbkkojgobmljda)
## GNOME Theming
- Ant Themes
- Sweet
- Colloid
- Nordic
- Orchis
## GNOME Apps
- GNOME Calculator
- GNOME Music
- GNOME Shortwave `WebRadio`
- GNOME Amberol `GUI Music Player`
- GNOME Dialect `Translation`
- GNOME Fragments `Torrenting`
- GNOME Identity `File Comparision`
- GNOME Drawing `Paint For Linux`
- GNOME Text Pieces `Text Manipulation`
## GNOME Extensions
- Dash to Panel
- Mute spotify ads
- Place Status Indicator
- spotify-tray
- Tray Icons: Reloaded
- User Avatar  in Quick Settings
- User Themes
- Search Light
- Quick Settings Tweaker
- Emoji Selector
## Flatpak & Elementory OS
### Add Elementory OS Repo to Flatpak
```
flatpak remote-add --system appcenter https://flatpak.elementary.io/repo.flatpakrepo
```
- Frog `OCR`
- Annotator `Drawing App`
- Metadata Cleaner `Clean metadata for files`
## Others
- Vimwiki
- Vieb (Vim-like browser)
- Raven Reader (news feed)
- toipe (CLI Typing Test)
- GPick (Colour Picker)

##  UPDATE IN PROGRESS KDE Apps 
Kalendar: tasks and appointments
Plan: Project management
Calligra Suite: Office Suite
Skrooge: Personal Budgeting
Kdenlive: Video Editing
Krita: Digital Painting
Kolourpaint: Paint, but for KDE
Elisa: Local Music Player
Kasts: Podcast Player
KDE Connect: Device integration
Subtitle Composer: create subtitles
Yakuake: drop down terminal
Sweeper: privacy and disk cleaner
Krusader: Power File Management
Falkon, KDE Itinerary, Marble, Artikulate, KAlgebra...
Krusader
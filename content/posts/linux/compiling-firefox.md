---
title: "Compiling Firefox"
date: "2022-10-25"
author: "Wilson Chang"
description: "Guidance on how to compile Firefox from source."
---

Guidance on how to compile Firefox from source. 

{{< image src="/image/compiling-firefox.jpg" alt="Failed to load image." position="center" style="border-radius: 8px;" >}}

## Code

```
# Install dependency: mercurial
sudo apt install mercurial
sudo pacman -S mercurial

# Compile Firefox
hg clone https://hg.mozilla.org/mozilla-central/
cd mozilla-central/
./mach bootstrap
./mach build
./mach run
```

---
title: "Things To Do After Installing Fedora"
date: "2022-10-27"
author: "Wilson Chang"
description: "This guide shows you some essential things to do after installing Fedora Workstation."
---

This guide shows you some essential things to do after installing Fedora Workstation.

{{< image src="/image/things-to-do-after-installing-fedora.jpg" alt="Failed to load image." position="center" style="border-radius: 8px;" >}}

## 1. Update System
Always update your system after installing a fresh OS.
```
sudo dnf update
```

## 2. Enable RPM Fusion
>### Free RPM Fusion
>This only enables free RPM Fusion repositories on your Fedora system.
>
>```
>sudo rpm -Uvh http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
>```
>
>#### Non-Free RPM Fusion
>This enables all non-free RPM Fusion in Fedora.
>```
>sudo rpm -Uvh http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E%fedora).noarch.rpm
>```

## 3. Enable Flathub
Flatpak is installed by default however flatpak isn't enabled. To do so, run the following command:
```
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

## 4. Improve DNF Speed
Edit `/etc/dnf/dnf.conf` and insert the following command.
```
fastestmirror=true
max_parallel_downloads=10
defaultyes=True
keepcache=True
```

## 5. Install Feddy
Feddy allows you to install additional software using post-installation script. Codecs, drivers, additional software can be installed using Feddy.
```
sudo dnf copr enable kwizart/fedy
sudo dnf install fedy -y
```

## 6. Change Hostname
This is totally optional, you may change it by using command:
```
sudo hostnamectl set-hostename YOUR_HOSTNAME
```

## 7. Install Media Codecs
```
sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
```

## 8. Clean DNF
It is better to clean unused or unwanted stuff for DNF.
```
sudo dnf clean all
```
----
## Reference
- https://youtu.be/DEAzqHz9LPs
- https://youtu.be/RrRpXs2pkzg

---
title: "Guide to Install Arch Linux"
date: "2022-11-19"
author: "Wilson Chang"
description: "Installation guide for Arch Linux (terminal installation method) on EFI system."
---

Installation guide for Arch Linux (terminal installation method) on EFI system.

## Edit Pacman Configuration FIle
```
nano /etc/pacman.conf
```

## System Clock
```
timedatectl set-ntp true
```

## Disk Partition

### List Disk
```
lsblk
```
### Partitioning
```
cfdisk
```
### Format Partition
```
mkfs.vfat /dev/boot-partition
mkfs.ext4 /dev/root-partition
mkswap /dev/swap-partition
```
### Activate Swap Partition (If You Have One)
```
swapon /dev/swap-partition
```

## Mount Partition
```
mount /dev/root-partition /mnt
mkdir /mnt/boot
mount /dev/boot-partition /mnt/boot
```

## Install Base System
```
pacstrap /mnt base base-devel linux linux-firmware git vim sudo wget archlinux-keyring htop neofetch pacman-contrib networkmanager
```

## Generate fstab
```
genfstab -U /mnt >> /mnt/etc/fstab
```

## Install & Configure Bootloader

### Installing Bootloader & Get Block ID
```
bootctl install --esp-path=/mnt/boot
PARTID=$(blkid -s PARTUUID -o value /dev/root-partition)
```
### Configure loader.conf
```
cat <<EOF > /mnt/boot/loader/loader.conf
default arch
timeout 4
EOF
```
### Configure Entries Directory
```
cat <<EOF > /mnt/boot/loader/entries/arch.conf
title Arch Linux
linux /vmlinuz-linux
initrd /initramfs-linux.img
options root=PARTUUID=$PARTID rw 
EOF
```

### Edit Newly Installed System's Pacman Configuration
```
cp /etc/pacman.conf /mnt/etc/pacman.conf
```

## Chroot Into Newly Installed System
```
arch-chroot /mnt
```

## Time Zone
```
ln -sf /usr/share/zoneinfo/Asia/Singapore /etc/localtime
hwclock --systohc
```

## Localisation
```
sed -i 's/^#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
```
### Create /etc/locale.conf
```
cat <<EOF > /etc/locale.conf
LANG=en_US.UTF-8
EOF
```

## Hostname
```
echo YOUR_HOSTNAME > /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 YOUR_HOSTNAME.localdomain YOUR_HOSTNAME" >> /etc/hosts
```

## Root Password (Optional)
```
passwd
```

## Add User
```
useradd -m USERNAME
passwd USERNAME
usermod -aG wheel,audio,storage,video,optical USERNAME
```

## Edit Sudoers File
```
sed -i 's/^%wheel ALL=(ALL) NOPASSWD: ALL/# %wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
sed -i 's/^%wheel ALL=(ALL:ALL) NOPASSWD: ALL/# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/' /etc/sudoers
sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
sed -i 's/^# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' /etc/sudoers
```

## Post Configuration (Optional)
```
pacman -S dhcpcd dhcp firefox chromium
systemctl enable dhcpcd
```

## Reboot
`reboot`
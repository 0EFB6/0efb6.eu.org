---
title: "Compiling Linux Kernel"
date: "2022-10-25"
author: "Wilson Chang"
description: "Guidance on how to compile Linux Kernel."
---

Guidance on how to compile Linux Kernel.

{{< image src="/image/compiling-linux-kernel.webp" alt="Failed to load image." position="center" style="border-radius: 8px;" >}}

## Code

```
# Install Prerequisites On Arch Linux
sudo pacman -S base-devel xmlto kmod inetutils bc libelf

# Configuration
make mrproper
make menuconfig (Do ur configuration stuff!!!)

# Compile
make
sudo make modules_install 
sudo cp -v arch/x86/boot/bzImage /boot/vmlinuz-YourKernelName
sudo mkinitcpio -k FullKernelName -c /etc/mkinitcpio.conf -g /boot/initramfs-YourKernelName.img
sudo cp System.map /boot/System.map-YourKernelName
```

---
title: "Virt Manager on Debian"
date: "2022-10-25"
author: "Wilson Chang"
description: "Guidance on how to install QEMU & Virt Manager in Debian GNU/Linux."
---

Guidance on how to install QEMU & Virt Manager in Debian GNU/Linux.

{{< image src="/image/virt-manager-debian.png" alt="Failed to load image." position="center" style="border-radius: 8px;" >}}

## Code

```
sudo apt install qemu-kvm libvirt-clients libvirt-daemon libvirt-daemon-system bridge-utils virtinst virt-manager
sudo systemctl enable libvirtd
sudo systemctl start libvirtdThere are several shortcuts key that help speed your command typing!There are several shortcuts key that help speed your command typing!
sudo adduser $USER libvirt
sudo adduser $USER libvirt-qemu
```

## Reference Video
- https://www.youtube.com/watch?v=a7Bx4T5GvOs


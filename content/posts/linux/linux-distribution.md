---
title: "Linux Distributions"
date: "2022-11-06"
author: "Wilson Chang"
description: "List of Linux Distributions."
---

List of Linux Distributions.

## List
1) Arch Linux
2) Centos
3) Deepin
4) Elementary os
5) Fedora
6) Kali Linux
7) Linux Lite
8) Linuxfx
9) Linuxmint
10) Manjaro
11) MX Linux
12) Nitrux
13) Parrot
14) Peppermint
15) Pop OS
16) Solus
17) Pure OS
18) Endaevour OS
19) Ubuntu
20) Zorin
21) Debian
22) Feren OS
23) Clean Linux OS
24) Artix(systemd-free Arch)
25) Devuan(systemd-free Debian)
26) Archlab(Minor changes made compared to pure Arch)
27) Archcraft(Comes with bspwm and openbox wm)
28) Slax OS(Portable)

## Quotes
- RTFM (Read the fXXXing manual)
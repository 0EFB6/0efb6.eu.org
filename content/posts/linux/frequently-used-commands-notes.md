---
title: "Frequently Used Commands"
date: "2022-10-25"
author: "Wilson Chang"
description: "Personal commands that are used frequently."
---

Personal commands that are used frequently.

## Handbrake-CLI
```
HandBrakeCLI -i input-file -o output-file -e x264 -q 31 -B 160 -v -f av_mkv --vfr
```

## Shred
```
shred -n 4 -v -z --remove *
```

---
title: "DWM on Debian"
date: "2022-10-25"
author: "Wilson Chang"
description: "Guidance on how to install DWM Window Manager on Debian GNU/Linux."
---

Guidance on how to install DWM Window Manager on Debian GNU/Linux.

{{< image src="/image/dwm-debian.png" alt="Failed to load image." position="center" style="border-radius: 8px;" >}}

## Code

```
# Prerequisite
sudo apt install xorg build-essential libx11-dev libxft-dev libxinerama-dev feh

# Recommended Programs To Be Installed
sudo apt install lxappearance gpm papirus-icon-theme ranger feh rxvt-unicode neofetch vim nano build-essential cmake p7zip p7zip-full unrar-free unzip wget curl dmenu ffmpeg obs-studio pcmanfm adb fastboot

sudo apt install pulseaudio pulseaudio-utils pavucontrol pulseaudio-dlna pulseaudio-equalizer gstreamer1.0-pulseaudio alsa-utils gstreamer1.0-alsa alsamixergui alsaplayer-gtk alsa-player-daemon alsa-player-common alsa-player-alsa libao-common libao-dev libao4 libasound2 libasound2-data libasound2-dev libasound2-doc libasound2-plugins exfat-utils
```

---
title: "Bash Shell Notes"
date: "2022-10-25"
author: "Wilson Chang"
description: "Short notes / cheatsheet for Bash Shell. In Bash, there are several shortcuts key that help speed your command typing!"
---

Short notes / cheatsheet for Bash Shell. In Bash, there are several shortcuts key that help speed your command typing!

{{< image src="/image/bash-notes.jpeg" alt="Failed to load image." position="center" style="border-radius: 8px;" >}}

## Keybindings
| Keybind | Description |   
| ----------- | ----------- |
| Ctrl + a | Moves cursor to the start of the line |  
| Ctrl + e | Moves cursor to the end of line |
| Ctrl + u | Remove all the texts before cursor|
| Ctrl + d | Ends a line or delete a character after the cursor |
| Ctrl + n | Searches the command history downwards |
| Ctrl + p | Searches the command history upwards |
| Ctrl + r | Reverse search |
| Ctrl + d | Signal bash that there is no more input |
| Ctrl + l | Clear screen |
| Ctrl + y | Paste deleted content |
| Ctrl + k | Remove all the texts after  cursor |
| Ctrl + x + e | Open current text in default editor |
| Alt + Backspace | Remove word |
| !! | Reuse the command typed |

## Commands
| Commands | Description |   
| ----------- | ----------- |
| pushd/popd | Put working directory on a stack |
| file | Determine file type |
| whatis | Display the on-line manual descriptions |
| apropos | Search the manual page names and descriptions |
| more/less | File perusal filter for crt viewing |
| users | Print the user names of users currently logged in |
| id | Print real and effective user and group IDs |
| killall | Kill processes by name |

## Refernce
- https://strftime.org/
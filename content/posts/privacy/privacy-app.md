---
title: "Privacy or Alternative Apps"
date: "2022-11-06"
author: "Wilson Chang"
description: "List of alternative and private apps."
---

List of alternative and private apps.

## Youtube
- Peertube
- LBRY/Odysee
## Search Engine
- Searx
- Duckduckgo
- Bravesearch
- Startpage
## Cloud Storage
- Treasure Cloud Services
- Nextcloud
- Proton Drive
## URL Shorterner
- Kutt.it
---
title: "Tmp"
date: "2022-10-30"
author: "Wilson Chang"
---

# Presentation
- https://gamma.app/public/MTH1114-COMPUTER-MATHEMATICS-ntwcua6ijo7jj4k

# Temporary References
- https://mpiatkowski.medium.com/encrypting-home-partition-on-an-already-installed-linux-machine-a738b668931a
- Fedora 25
- Gentoo 20
- Self Host Server
https://www.howtogeek.com/846979/heres-why-self-hosting-a-server-is-worth-the-effort/

Tesseract OCR
- https://medium.com/better-programming/beginners-guide-to-tesseract-ocr-using-python-10ecbb426c3d

Chess
- https://lichess.org/

Arch Linux GUI
- https://archlinuxgui.in/
- https://sourceforge.net/projects/arch-linux-gui/
- https://github.com/arch-linux-gui

RSS
- https://christitus.com/why-we-dont-browse-the-internet-anymore/

Virt Manager Share Host File
- https://blog.sergeantbiggs.net/posts/file-sharing-with-qemu-and-virt-manager/

Crack Spotify
- https://crackshash.org/spotify-crack-ad-free-cracked/
- https://rexdlbox.com/index.php?id=spotify-premium-apk